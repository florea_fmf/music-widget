/** data.h -- various banshee/dbus functions
	* version 1.0, September 9th, 2012
	*
	* Copyright (C) 2012 Florea Marius Florin
	*
	* This software is provided 'as-is', without any express or implied
	* warranty.  In no event will the authors be held liable for any damages
	* arising from the use of this software.
	*
	* Permission is granted to anyone to use this software for any purpose,
	* including commercial applications, and to alter it and redistribute it
	* freely, subject to the following restrictions:
	*
	*		1. The origin of this software must not be misrepresented; you must not
	*			claim that you wrote the original software. If you use this software
	*			in a product, an acknowledgment in the product documentation would be
	*			appreciated but is not required.
	*		2. Altered source versions must be plainly marked as such, and must not be
	*			misrepresented as being the original software.
	*		3. This notice may not be removed or altered from any source distribution.
	*
	* Florea Marius Florin, florea.fmf@gmail.com
	*/

/** unit that is responsable for talking do dbus
  * and getting the required information about
  * the currently playing song and player status
  */

#ifndef DATA_H_INCLUDED
#define DATA_H_INCLUDED


#include <gio/gunixfdlist.h>


/** structure need it when scrolling the labels.
  * it contains information about the position
  * at which the label is scrolled. Need two:
  * one for scrolling left->rigth, the other
  * for scrolling rigth->left
  */
typedef struct {
	gint name, name2;
	gint artist, artist2;
	gint album, album2;
} position_t;

/** structure containeg the info of the track
  */
typedef struct {
	gchar *name, *artist, *album;
	gchar *genre, *artworkId;
	gint year, rating, playcount;
	gdouble length, position;
	/* used to signal if the track has changed, in an attemp
	 * to try to remove some unnecessry calls;
	 * if "name" != "oldName" --> track has changed
	 * (not the best way to check this but hope it does the job)
	 * IMPORTANT: "oldName" must be freed manually before the
	 * program quits (put a "g_free(track.track.oldName)"
										(or whatever you need) after "gtk_main();) */
	gboolean changed;
	gchar *oldName;
	/* used to see if any of those should be scrolled.
	 * if length is higher than the SCROLL_SIZE_W */
	gint nameLen, artistLen, albumLen;
	position_t x;
	/* the number assigned by g_timeout_add func; used to
	 * stop the timeout when the label need no longer be scrolled */
	guint nameTag, artistTag, albumTag;
	/* set to true which of them needs to be scrolled */
	gboolean nameScroll, artistScroll, albumScroll;
} trackInfo_t;

/** various info about player status
	*/
typedef struct {
	/* current player state. Note that I'm interested to see
	 * if the player is playing or not, all other states
	 * except playing will be flaged as stopped.
	 * Note: 1 means it is plying, 0 it's not
	 * shuffle works the same as status. the shuffle method
	 * used is "by song" */
	gboolean status, shuffle;
	/* current repeat mode:
	 * 		0 = off;
	 *		1 = all;
	 *		2 = single; */
	gint repeat;
	/* problaby the dumbest idea of "optimization" ever, but here it goes:
	 * so we have to check for the player status less often then that of
	 * the track info (because who the fuck fucks with the player controls
	 *									every half a second?). My idea is to have a counter
	 * and only check/update the player controls when the counter reaches
	 * a certain amount (like 2 seconds (most probably)). This is what the
	 * bellow var is for.
	 * p.s.: now you can laugh
	 * p.s.s.: stupid as shit but decreases the load on cpu quite a lot */
	 gint counter;
} playerInfo_t;


/** opens a connection to dbus
  * returns succes/failure
  */
gboolean connect_to_dbus(void);

/** closes the previous open connection to dbus
  * returns succes/failure
  */
gboolean disconnect_from_dbus(void);

/** checks to see if the player is running
  * returns 1 if it does, 0 otherwise
  */
gboolean player_running(void);

/** gets the current track info
  * returns succes/failure
  */
gboolean get_track_info(trackInfo_t *t);

/** dealoctates the memory holded by the track
  */
void free_track_info(trackInfo_t *t);

/** returns info about the player
  */
gboolean get_player_info(playerInfo_t *i);

/** pauses the current track
	* returns succes/failure
  */
gboolean set_player_pause(void);
/** continues playing from last position, or last track
  * returns succes/failure
  */
gboolean set_player_play(void);

/** skips to next track in playlist
  * returns succes/failure
  */
gboolean set_player_next(void);

/** skips to the previous track in playlist
  * returns succes/failure
  */
gboolean set_player_prev(void);

/** sets the next repeat mode.
  * if no repeat --> sets repeat all
  * if repeat all --> sets repeat single
  * if repeat single --> sets no repeat
  * returns succes/failure
  */
gboolean set_player_repeat(gint data);

/** sets the shuffle on/off
  * returns succes/failure
  */
gboolean set_player_shuffle(gchar *data);

/** opens banshee window when clicked on
  * the image with the album artwork
  * returns succes/failure
  */
gboolean show_player(void);

/** checks to see if data is valid utf-8
	* returns succes/failure
	*/
gboolean validate_track_info(trackInfo_t *t);

#endif
