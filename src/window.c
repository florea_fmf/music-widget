/** window.c -- functions implementations for window.h
	* version 1.0, September 9th, 2012
	*
	* Copyright (C) 2012 Florea Marius Florin
	*
	* This software is provided 'as-is', without any express or implied
	* warranty.  In no event will the authors be held liable for any damages
	* arising from the use of this software.
	*
	* Permission is granted to anyone to use this software for any purpose,
	* including commercial applications, and to alter it and redistribute it
	* freely, subject to the following restrictions:
	*
	*		1. The origin of this software must not be misrepresented; you must not
	*			claim that you wrote the original software. If you use this software
	*			in a product, an acknowledgment in the product documentation would be
	*			appreciated but is not required.
	*		2. Altered source versions must be plainly marked as such, and must not be
	*			misrepresented as being the original software.
	*		3. This notice may not be removed or altered from any source distribution.
	*
	* Florea Marius Florin, florea.fmf@gmail.com
	*/

#include "window.h"
#include "icons.h" /* getting multiple definition if included in window.h (no idea why) */


/** codes to know which of the control buttons was pressed
	*/
#define BUTTON_PLAY_PAUSE 1
#define BUTTON_PREV 2
#define BUTTON_NEXT 3
#define BUTTON_REPEAT 4
#define BUTTON_SHUFFLE 5


/** time is returned from banshee in seconds
  * this function transform that number in 2 parts
  * (minutes and seconds as "min:sec")
  */
static void format_time(gchar dest[10], gdouble nr)
{
	gint i = 0;


	while (nr > 60.0) {
		i++;
		nr -= 60.0;
	}

	if (fabs(nr-60.0) < 0.01)
		g_sprintf(dest, "%d:00", i+1);
	else if (nr > 9.0)
		g_sprintf(dest, "%d:%.0f", i, nr);
	else
		g_sprintf(dest, "%d:0%.0f", i, nr);

	return;
}


static gboolean scroll_label_name(track_t *t)
{
	gint len = 0;


	len = t->track.nameLen-SCROLL_SIZE_W;
	if (t->track.x.name > -len) {
		gtk_layout_move(GTK_LAYOUT(t->trackw.layout.name), t->trackw.label.name, --t->track.x.name, 0);
		if (t->track.x.name > -len+1)
			t->track.x.name2 = -len;
	} else {
		gtk_layout_move(GTK_LAYOUT(t->trackw.layout.name), t->trackw.label.name, ++t->track.x.name2, 0);
		if (t->track.x.name2 == 1)
			t->track.x.name = 0;
	}

	return TRUE;
}


static gboolean scroll_label_artist(track_t *t)
{
	gint len = 0;


	len = t->track.artistLen-SCROLL_SIZE_W+23;
	if (t->track.x.artist > -len) {
		gtk_layout_move(GTK_LAYOUT(t->trackw.layout.artist), t->trackw.label.artist, --t->track.x.artist, 0);
		if (t->track.x.artist > -len+1)
			t->track.x.artist2 = -len;
	} else {
		gtk_layout_move(GTK_LAYOUT(t->trackw.layout.artist), t->trackw.label.artist, ++t->track.x.artist2, 0);
		if (t->track.x.artist2 == 1)
			t->track.x.artist = 0;
	}

	return TRUE;
}


static gboolean scroll_label_album(track_t *t)
{
	gint len = 0;


	len = t->track.albumLen-SCROLL_SIZE_W+40;
	if (t->track.x.album > -len) {
		gtk_layout_move(GTK_LAYOUT(t->trackw.layout.album), t->trackw.label.album, --t->track.x.album, 0);
		if (t->track.x.album > -len+1)
			t->track.x.album2 = -len;
	} else {
		gtk_layout_move(GTK_LAYOUT(t->trackw.layout.album), t->trackw.label.album, ++t->track.x.album2, 0);
		if (t->track.x.album2 == 1)
			t->track.x.album = 0;
	}

	return TRUE;
}


gboolean update_track_info(track_t *t)
{
	gchar dummy[10], *markup = NULL, *markupProps = NULL;
	gint i = 0;
	GdkPixbuf *starPix = NULL;
	PangoLayout *layout = NULL;


	get_track_info(&t->track);
	if (!validate_track_info(&t->track)) {
		g_print("Malformed track data\n");
		gtk_main_quit();
	}


	if (t->track.changed) {
		gtk_image_set_from_file(GTK_IMAGE(t->trackw.image), t->track.artworkId);


		markupProps = g_strdup("<span font='Sans 11' font_weight='bold' color='#FFFFFF'>%s</span>");


		t->track.x.name = 0;
		t->track.x.name2 = 0;

		markup = g_markup_printf_escaped(markupProps, t->track.name);
		gtk_label_set_markup(GTK_LABEL(t->trackw.label.name), markup);
		layout = gtk_label_get_layout(GTK_LABEL(t->trackw.label.name));
		pango_layout_get_pixel_size(PANGO_LAYOUT(layout), &t->track.nameLen, NULL);

		if (t->track.nameLen > SCROLL_SIZE_W) {
			if (t->track.nameScroll)
				g_source_remove(t->track.nameTag);
			t->track.nameScroll = TRUE;
			t->track.nameTag = g_timeout_add(SCROLL_SPEED, (GSourceFunc) scroll_label_name, t);
		} else if (t->track.nameScroll) {
			/* if the previous label was scrolled and this one doesn't need do
			 * it stops the scrolling and puts the label at it's origin. Failure to do
			 * so causes the new label to be displayed at the last position the previous
			 * label was scrolled. */
			g_source_remove(t->track.nameTag);
			gtk_layout_move(GTK_LAYOUT(t->trackw.layout.name), t->trackw.label.name, 0, 0);
			t->track.nameScroll = FALSE;
		}
		g_free(markup);


		t->track.x.artist = 0;
		t->track.x.artist2 = 0;

		markup = g_markup_printf_escaped(markupProps, t->track.artist);
		gtk_label_set_markup(GTK_LABEL(t->trackw.label.artist), markup);
		layout = gtk_label_get_layout(GTK_LABEL(t->trackw.label.artist));
		pango_layout_get_pixel_size(PANGO_LAYOUT(layout), &t->track.artistLen, NULL);

		if (t->track.artistLen > SCROLL_SIZE_W-23) {
			if (t->track.artistScroll)
				g_source_remove(t->track.artistTag);
			t->track.artistScroll = TRUE;
			t->track.artistTag = g_timeout_add(SCROLL_SPEED, (GSourceFunc) scroll_label_artist, t);
		} else if (t->track.artistScroll) {
			g_source_remove(t->track.artistTag);
			gtk_layout_move(GTK_LAYOUT(t->trackw.layout.artist), t->trackw.label.artist, 0, 0);
			t->track.artistScroll = FALSE;
		}
		g_free(markup);


		t->track.x.album = 0;
		t->track.x.album2 = 0;

		markup = g_markup_printf_escaped(markupProps, t->track.album);
		gtk_label_set_markup(GTK_LABEL(t->trackw.label.album), markup);
		layout = gtk_label_get_layout(GTK_LABEL(t->trackw.label.album));
		pango_layout_get_pixel_size(PANGO_LAYOUT(layout), &t->track.albumLen, NULL);

		if (t->track.albumLen > SCROLL_SIZE_W-40) {
			if (t->track.albumScroll)
				g_source_remove(t->track.albumTag);
			t->track.albumScroll = TRUE;
			t->track.albumTag = g_timeout_add(SCROLL_SPEED, (GSourceFunc) scroll_label_album, t);
		} else if (t->track.albumScroll) {
			g_source_remove(t->track.albumTag);
			gtk_layout_move(GTK_LAYOUT(t->trackw.layout.album), t->trackw.label.album, 0, 0);
			t->track.albumScroll = FALSE;
		}
		g_free(markup);
		g_free(markupProps);


		markupProps = g_strdup("<span font='Sans 11' color='#FFFFFF'>%s</span>");
		markup = g_markup_printf_escaped(markupProps, t->track.genre);
		gtk_label_set_markup(GTK_LABEL(t->trackw.genre), markup);
		g_free(markup);

		g_sprintf(dummy, "%d", t->track.year);
		markup = g_markup_printf_escaped(markupProps, dummy);
		gtk_label_set_markup(GTK_LABEL(t->trackw.year), markup);
		g_free(markup);
		dummy[0] = '\0';

		format_time(dummy, t->track.length);
		markup = g_markup_printf_escaped(markupProps, dummy);
		gtk_label_set_markup(GTK_LABEL(t->trackw.length), markup);
		g_free(markup);
		g_free(markupProps);
		dummy[0] = '\0';


		gtk_range_set_range(GTK_RANGE(t->trackw.slider), 0, t->track.length);

		/* set the rating */
		starPix = gdk_pixbuf_new_from_xpm_data(starFull_xpm);
		for (i = 1; i <= t->track.rating; i++)
			gtk_image_set_from_pixbuf(GTK_IMAGE(t->trackw.stars[i-1]), GDK_PIXBUF(starPix));
		g_object_unref(starPix);
		starPix = gdk_pixbuf_new_from_xpm_data(starHollow_xpm);
		while (i <= 5) {
			gtk_image_set_from_pixbuf(GTK_IMAGE(t->trackw.stars[i-1]), GDK_PIXBUF(starPix));
			i++;
		}
		g_object_unref(starPix);
	}


	markupProps = g_strdup("<span font='Sans 11' color='#FFFFFF'>%s</span>");

	g_sprintf(dummy, "%d", t->track.playcount);
	markup = g_markup_printf_escaped(markupProps, dummy);
	gtk_label_set_markup(GTK_LABEL(t->trackw.playcount), markup);
	g_free(markup);
	dummy[0] = '\0';

	format_time(dummy, t->track.position);
	markup = g_markup_printf_escaped(markupProps, dummy);
	gtk_label_set_markup(GTK_LABEL(t->trackw.position), markup);
	g_free(markup);
	g_free(markupProps);
	dummy[0] = '\0';


	gtk_range_set_fill_level(GTK_RANGE(t->trackw.slider), t->track.position);
	gtk_range_set_value(GTK_RANGE(t->trackw.slider), t->track.position);


	/* NOTE: reuses the starPix pixbuf */
	if (t->playerInfo.counter == 4) { /* 2 seconds on currently UPDATE_SPEED */
		if (!get_player_info(&t->playerInfo))
				return FALSE;
		else {
			if (t->playerInfo.status) {
				starPix = gdk_pixbuf_new_from_xpm_data(pause_xpm);
				gtk_image_set_from_pixbuf(GTK_IMAGE(t->playerControls.playPause), starPix);
				g_object_unref(starPix);
			} else {
				starPix = gdk_pixbuf_new_from_xpm_data(play_xpm);
				gtk_image_set_from_pixbuf(GTK_IMAGE(t->playerControls.playPause), starPix);
				g_object_unref(starPix);
			}

			if (t->playerInfo.repeat == 0) {
				starPix = gdk_pixbuf_new_from_xpm_data(repeatOff_xpm);
				gtk_image_set_from_pixbuf(GTK_IMAGE(t->playerControls.repeat), starPix);
				g_object_unref(starPix);
			} else if (t->playerInfo.repeat == 1) {
				starPix = gdk_pixbuf_new_from_xpm_data(repeatAll_xpm);
				gtk_image_set_from_pixbuf(GTK_IMAGE(t->playerControls.repeat), starPix);
				g_object_unref(starPix);
			} else {
				starPix = gdk_pixbuf_new_from_xpm_data(repeatSingle_xpm);
				gtk_image_set_from_pixbuf(GTK_IMAGE(t->playerControls.repeat), starPix);
				g_object_unref(starPix);
			}

			if (t->playerInfo.shuffle) {
				starPix = gdk_pixbuf_new_from_xpm_data(shuffleOn_xpm);
				gtk_image_set_from_pixbuf(GTK_IMAGE(t->playerControls.shuffle), starPix);
				g_object_unref(starPix);
			} else {
				starPix = gdk_pixbuf_new_from_xpm_data(shuffleOff_xpm);
				gtk_image_set_from_pixbuf(GTK_IMAGE(t->playerControls.shuffle), starPix);
				g_object_unref(starPix);
			}
		}

		t->playerInfo.counter = 0;
	}


	free_track_info(&t->track);
	t->playerInfo.counter++;

	return TRUE;
}



/*static void slider_value_changed(GtkRange *range)
{
	gdouble dummy = 0.0;


	dummy = gtk_range_get_fill_level(range);
	gtk_range_set_fill_level(range, dummy);
	g_print("%0.2f\n", dummy);

	return;
} */


/* takes proper action depending on what button was pressed and
 * the state of the player */
static void button_callback(GtkWidget *widget, gint *btnId)
{
	/* how do I always end up using the stupidest solutions I
	 * can think of? */
	playerInfo_t info;


	get_player_info(&info);
	switch (GPOINTER_TO_INT(btnId)) {
		case BUTTON_PLAY_PAUSE : if (info.status)
																set_player_pause();
														 else
														 		set_player_play();
														 break;

		case BUTTON_PREV			 : set_player_prev(); break;

		case BUTTON_NEXT			 : set_player_next(); break;

		case BUTTON_REPEAT		 : if (info.repeat == 0)
																set_player_repeat(1);
														 else if (info.repeat == 1)
														 		set_player_repeat(2);
														 else
														 		set_player_repeat(0);
														 break;

		case BUTTON_SHUFFLE		 : if (info.shuffle)
																set_player_shuffle("off");
														 else
														 		set_player_shuffle("song");
														 break;

		default								 : g_print("dafuq?\n"); break;
	}

	return;
}


static gboolean show_player_callback(GtkWidget *eventBox, GdkEventButton *event, gpointer data)
{
	show_player();

	return TRUE; /* to stop the signal emission */
}


gboolean set_widgets_defaults(GtkWidget *mvbox, track_t *t, GdkRGBA *color)
{
	GtkWidget *hbox, *vbox, *label, *hbox2,
						*sep, *button, *img, *eventBox;
	gint i = 0;
	GdkRGBA buttonNormal = {1, 1, 1, 0.2};
	GdkRGBA buttonSelected = {1, 1, 1, 0.6};


	hbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 10);
	gtk_box_pack_start(GTK_BOX(mvbox), hbox, 0, 0, 1);

	/* album image */
	eventBox = gtk_event_box_new();
	gtk_event_box_set_visible_window(GTK_EVENT_BOX(eventBox), 0);
	gtk_widget_set_events(eventBox, GDK_BUTTON_PRESS_MASK);

	t->trackw.image = gtk_image_new();
	gtk_container_add(GTK_CONTAINER(eventBox), t->trackw.image);
	g_signal_connect(eventBox, "button_press_event", G_CALLBACK(show_player_callback), NULL);
	gtk_box_pack_start(GTK_BOX(hbox), eventBox, 0, 0, 0);


	/* track info: name, artist, album */
	vbox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
	gtk_box_pack_start(GTK_BOX(hbox), vbox, 0, 0, 0);


	t->trackw.label.name = gtk_label_new(NULL);
	gtk_misc_set_alignment(GTK_MISC(t->trackw.label.name), 0, 0);
	/* create the layout to be able to scrol the label's contents */
	t->trackw.layout.name = gtk_layout_new(NULL, NULL);
	/* set size otherwise it won't show up */
	gtk_widget_set_size_request(t->trackw.layout.name, SCROLL_SIZE_W, SCROLL_SIZE_H);
	/* make layout's color the same as the window's */
	gtk_widget_override_background_color(t->trackw.layout.name, GTK_STATE_FLAG_NORMAL, color);
	gtk_layout_put(GTK_LAYOUT(t->trackw.layout.name), t->trackw.label.name, 0, 0);


	t->trackw.label.artist = gtk_label_new(NULL);
	gtk_misc_set_alignment(GTK_MISC(t->trackw.label.artist), 0, 0);
	t->trackw.layout.artist = gtk_layout_new(NULL, NULL);
	gtk_widget_set_size_request(t->trackw.layout.artist, SCROLL_SIZE_W-23, SCROLL_SIZE_H);
	gtk_widget_override_background_color(t->trackw.layout.artist, GTK_STATE_FLAG_NORMAL, color);
	gtk_layout_put(GTK_LAYOUT(t->trackw.layout.artist), t->trackw.label.artist, 0, 0);


	t->trackw.label.album = gtk_label_new(NULL);
	gtk_misc_set_alignment(GTK_MISC(t->trackw.label.album), 0, 0);
	t->trackw.layout.album = gtk_layout_new(NULL, NULL);
	gtk_widget_set_size_request(t->trackw.layout.album, SCROLL_SIZE_W-40, SCROLL_SIZE_H);
	gtk_widget_override_background_color(t->trackw.layout.album, GTK_STATE_FLAG_NORMAL, color);
	gtk_layout_put(GTK_LAYOUT(t->trackw.layout.album), t->trackw.label.album, 0, 0);


	/* put the name, artist and album at their places */
	gtk_box_pack_start(GTK_BOX(vbox), t->trackw.layout.name, 0, 0, 0);


	hbox2 = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
	gtk_box_pack_start(GTK_BOX(vbox), hbox2, 0, 0, 0);

	label = gtk_label_new(NULL);
	gtk_misc_set_alignment(GTK_MISC(label), 0, 0);
	gtk_label_set_markup(GTK_LABEL(label), "<span font='Sans 11' font_style='italic'"
												"color='#FFFFFF'>by </span>");
	gtk_box_pack_start(GTK_BOX(hbox2), label, 0, 0, 0);
	gtk_box_pack_start(GTK_BOX(hbox2), t->trackw.layout.artist, 0, 0, 0);


	hbox2 = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
	gtk_box_pack_start(GTK_BOX(vbox), hbox2, 0, 0, 0);

	label = gtk_label_new(NULL);
	gtk_misc_set_alignment(GTK_MISC(label), 0, 0);
	gtk_label_set_markup(GTK_LABEL(label), "<span font='Sans 11' font_style='italic'"
												"color='#FFFFFF'>from </span>");
	gtk_box_pack_start(GTK_BOX(hbox2), label, 0, 0, 0);
	gtk_box_pack_start(GTK_BOX(hbox2), t->trackw.layout.album, 0, 0, 0);


	/* puts the genre, rating, year, and playcount */
	hbox2 = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
	gtk_box_pack_start(GTK_BOX(vbox), hbox2, 0, 0, 0);

	t->trackw.genre = gtk_label_new(NULL);
	gtk_misc_set_alignment(GTK_MISC(t->trackw.genre), 0, 0);
	gtk_box_pack_start(GTK_BOX(hbox2), t->trackw.genre, 0, 0, 0);


	sep = gtk_separator_new(GTK_ORIENTATION_VERTICAL);
	gtk_box_pack_start(GTK_BOX(hbox2), sep, 0, 0, 3);


	for (i = 0; i < 5; i++) {
		t->trackw.stars[i] = gtk_image_new();
		gtk_box_pack_start(GTK_BOX(hbox2), t->trackw.stars[i], 0, 0, 1);
	}


	sep = gtk_separator_new(GTK_ORIENTATION_VERTICAL);
	gtk_box_pack_start(GTK_BOX(hbox2), sep, 0, 0, 3);


	t->trackw.year = gtk_label_new(NULL);
	gtk_misc_set_alignment(GTK_MISC(t->trackw.year), 0, 0);
	gtk_box_pack_start(GTK_BOX(hbox2), t->trackw.year, 0, 0, 0);


	sep = gtk_separator_new(GTK_ORIENTATION_VERTICAL);
	gtk_box_pack_start(GTK_BOX(hbox2), sep, 0, 0, 3);


	t->trackw.playcount = gtk_label_new(NULL);
	gtk_misc_set_alignment(GTK_MISC(t->trackw.playcount), 0, 0);
	gtk_box_pack_start(GTK_BOX(hbox2), t->trackw.playcount, 0, 0, 0);


	/* puts the current position, the slider showing the progress
	 * and track length */
	hbox2 = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
	gtk_box_pack_start(GTK_BOX(vbox), hbox2, 0, 0, 0);


	t->trackw.position = gtk_label_new(NULL);
	gtk_misc_set_alignment(GTK_MISC(t->trackw.position), 0, 0);
	gtk_box_pack_start(GTK_BOX(hbox2), t->trackw.position, 0, 0, 0);


	t->trackw.slider = gtk_scale_new(GTK_ORIENTATION_HORIZONTAL, NULL);
	gtk_widget_set_size_request(t->trackw.slider, 170, -1);
	gtk_scale_set_draw_value(GTK_SCALE(t->trackw.slider), 0);
	gtk_range_set_show_fill_level(GTK_RANGE(t->trackw.slider), 1);
	gtk_range_set_restrict_to_fill_level(GTK_RANGE(t->trackw.slider), 0);
	/*g_signal_connect(t->trackw.slider, "value-changed", G_CALLBACK(slider_value_changed), NULL);*/
	gtk_box_pack_start(GTK_BOX(hbox2), t->trackw.slider, 0, 0, 0);


	t->trackw.length = gtk_label_new(NULL);
	gtk_misc_set_alignment(GTK_MISC(t->trackw.length), 0, 0);
	gtk_box_pack_start(GTK_BOX(hbox2), t->trackw.length, 0, 0, 0);


	/* the player controls */
	hbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 5);
	gtk_box_pack_start(GTK_BOX(mvbox), hbox, 0, 0, 0);


	img = gtk_image_new_from_pixbuf(gdk_pixbuf_new_from_xpm_data(prev_xpm));
	button = gtk_button_new();
	gtk_widget_override_background_color(button, GTK_STATE_FLAG_NORMAL, &buttonNormal);
	gtk_widget_override_background_color(button, GTK_STATE_FLAG_ACTIVE, &buttonSelected);
	gtk_container_add(GTK_CONTAINER(button), img);
	g_signal_connect(button, "clicked", G_CALLBACK(button_callback),
										GINT_TO_POINTER(BUTTON_PREV));
	gtk_box_pack_start(GTK_BOX(hbox), button, 0, 0, 0);


	t->playerControls.playPause = gtk_image_new();
	button = gtk_button_new();
	gtk_widget_override_background_color(button, GTK_STATE_FLAG_NORMAL, &buttonNormal);
	gtk_widget_override_background_color(button, GTK_STATE_FLAG_ACTIVE, &buttonSelected);
	gtk_container_add(GTK_CONTAINER(button), t->playerControls.playPause);
	g_signal_connect(button, "clicked", G_CALLBACK(button_callback),
										GINT_TO_POINTER(BUTTON_PLAY_PAUSE));
	gtk_box_pack_start(GTK_BOX(hbox), button, 0, 0, 0);


	img = gtk_image_new_from_pixbuf(gdk_pixbuf_new_from_xpm_data(next_xpm));
	button = gtk_button_new();
	gtk_widget_override_background_color(button, GTK_STATE_FLAG_NORMAL, &buttonNormal);
	gtk_widget_override_background_color(button, GTK_STATE_FLAG_ACTIVE, &buttonSelected);
	gtk_container_add(GTK_CONTAINER(button), img);
	g_signal_connect(button, "clicked", G_CALLBACK(button_callback),
										GINT_TO_POINTER(BUTTON_NEXT));
	gtk_box_pack_start(GTK_BOX(hbox), button, 0, 0, 0);


	sep = gtk_separator_new(GTK_ORIENTATION_VERTICAL);
	gtk_box_pack_start(GTK_BOX(hbox), sep, 0, 0, 2);

	t->playerControls.repeat = gtk_image_new();
	button = gtk_button_new();
	gtk_widget_override_background_color(button, GTK_STATE_FLAG_NORMAL, &buttonNormal);
	gtk_widget_override_background_color(button, GTK_STATE_FLAG_ACTIVE, &buttonSelected);
	gtk_container_add(GTK_CONTAINER(button), t->playerControls.repeat);
	g_signal_connect(button, "clicked", G_CALLBACK(button_callback),
										GINT_TO_POINTER(BUTTON_REPEAT));
	gtk_box_pack_start(GTK_BOX(hbox), button, 0, 0, 0);


	t->playerControls.shuffle = gtk_image_new();
	button = gtk_button_new();
	gtk_widget_override_background_color(button, GTK_STATE_FLAG_NORMAL, &buttonNormal);
	gtk_widget_override_background_color(button, GTK_STATE_FLAG_ACTIVE, &buttonSelected);
	gtk_container_add(GTK_CONTAINER(button), t->playerControls.shuffle);
	g_signal_connect(button, "clicked", G_CALLBACK(button_callback),
										GINT_TO_POINTER(BUTTON_SHUFFLE));
	gtk_box_pack_start(GTK_BOX(hbox), button, 0, 0, 0);

	return TRUE;
}


void on_screen_change_event(GtkWidget *widget, gboolean *supportsAlpha)
{
	GdkScreen *screen = gtk_widget_get_screen(widget);
	GdkVisual *visual = gdk_screen_get_rgba_visual(screen);


	if (!visual)
		g_print("Your screen doesn't support alpha\n");
	else
		*supportsAlpha = TRUE;

	gtk_widget_set_visual(widget, visual);

	g_object_unref(visual);

	return;
}


gboolean on_draw_event(GtkWidget *widget, gboolean *supportsAlpha)
{
	cairo_t *cr = gdk_cairo_create(gtk_widget_get_window(widget));


	if (supportsAlpha)
		cairo_set_source_rgba(cr, WINDOW_BG_COLOR, WINDOW_ALPHA);
	else
		cairo_set_source_rgb(cr, WINDOW_BG_COLOR);

	/* draw bg */
	cairo_set_operator(cr, CAIRO_OPERATOR_SOURCE);
	cairo_paint(cr);

	/* draw border */
	if (supportsAlpha)
		cairo_set_source_rgba(cr, 1, 1, 1, WINDOW_ALPHA);
	else
		cairo_set_source_rgb(cr, 1, 1, 1);

	cairo_set_line_width(cr, 4);
	cairo_rectangle(cr, 1, 1, WINDOW_WIDTH-1, WINDOW_HEIGHT+1);
	cairo_stroke(cr);
	cairo_fill(cr);

	cairo_set_line_width(cr, 2);
	cairo_move_to(cr, 0, 101);
	cairo_line_to(cr, WINDOW_WIDTH, 101);
	cairo_stroke(cr);

	cairo_destroy(cr);

	return FALSE;
}
