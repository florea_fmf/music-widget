/** data.c -- functions implementations for data.h
	* version 1.0, September 9th, 2012
	*
	* Copyright (C) 2012 Florea Marius Florin
	*
	* This software is provided 'as-is', without any express or implied
	* warranty.  In no event will the authors be held liable for any damages
	* arising from the use of this software.
	*
	* Permission is granted to anyone to use this software for any purpose,
	* including commercial applications, and to alter it and redistribute it
	* freely, subject to the following restrictions:
	*
	*		1. The origin of this software must not be misrepresented; you must not
	*			claim that you wrote the original software. If you use this software
	*			in a product, an acknowledgment in the product documentation would be
	*			appreciated but is not required.
	*		2. Altered source versions must be plainly marked as such, and must not be
	*			misrepresented as being the original software.
	*		3. This notice may not be removed or altered from any source distribution.
	*
	* Florea Marius Florin, florea.fmf@gmail.com
	*/

#include <stdlib.h>
#include "data.h"


/* the connection to dbus */
static GDBusConnection *connection = NULL;
/* the proxy to call banshee's dbus interface */
static GDBusProxy *trackInfoProxy = NULL;
static GDBusProxy *playerControlsProxy = NULL;


gboolean connect_to_dbus(void)
{
	GError *error = NULL;


	connection = g_bus_get_sync(G_BUS_TYPE_SESSION, NULL, &error);
	if (connection == NULL) {
		g_print("Failed to connect to DBus: %s\n", error->message);
		g_error_free(error);
		return FALSE;
	}


	trackInfoProxy = g_dbus_proxy_new_sync(connection, G_DBUS_PROXY_FLAGS_NONE, NULL,
																				"org.bansheeproject.Banshee",
																				"/org/bansheeproject/Banshee/PlayerEngine",
																				"org.bansheeproject.Banshee.PlayerEngine",
																				NULL, &error);
	if (trackInfoProxy == NULL) {
		g_print("Failed to get track info: %s\n", error->message);
		g_error_free(error);
		return FALSE;
	}


	playerControlsProxy = g_dbus_proxy_new_sync(connection, G_DBUS_PROXY_FLAGS_NONE, NULL,
																							"org.bansheeproject.Banshee",
																							"/org/bansheeproject/Banshee/PlaybackController",
																							"org.bansheeproject.Banshee.PlaybackController",
																							NULL, &error);
	if (playerControlsProxy == NULL) {
		g_print("Failed to get track info: %s\n", error->message);
		g_error_free(error);
		return FALSE;
	}

	return TRUE;
}


gboolean disconnect_from_dbus(void)
{
	GError *error = NULL;
	gboolean status = FALSE;


	status = g_dbus_connection_close_sync(G_DBUS_CONNECTION(connection), NULL, &error);
	if (!status) {
		g_print("Failed to disconnect from DBus: %s\n", error->message);
		g_error_free(error);
		return FALSE;
	}

	g_object_unref(trackInfoProxy);
	g_object_unref(playerControlsProxy);
	g_object_unref(connection);

	return TRUE;
}


gboolean player_running(void)
{
	GError *error = NULL;
	GDBusProxy *proxy = NULL;
	GVariant *nameList = NULL;
	GVariantIter *iter = NULL;
	gchar *str = NULL;
	gboolean isStarted = FALSE;


	proxy = g_dbus_proxy_new_sync(connection, G_DBUS_PROXY_FLAGS_NONE, NULL,
																"org.freedesktop.DBus", "/org/freedesktop/DBus",
																"org.freedesktop.DBus", NULL, &error);
	if (proxy == NULL) {
		g_print("Failed to get player status: %s\n", error->message);
		g_error_free(error);
		return FALSE;
	}

	nameList = g_dbus_proxy_call_sync(proxy, "ListNames", NULL,
																		G_DBUS_CALL_FLAGS_NONE, -1, NULL, &error);
	if (nameList == NULL) {
		g_print("Failed to get player status: %s\n", error->message);
		g_error_free(error);
		return FALSE;
	}

	g_variant_get(nameList, "(as)", &iter);
	while (g_variant_iter_loop(iter, "s", &str))
		if (g_strcmp0(str, "org.bansheeproject.Banshee") == 0) {
			isStarted = TRUE;
			break;
		}

	g_variant_iter_free(iter);
	g_variant_unref(nameList);
	g_free(str);
	g_object_unref(proxy);

	return isStarted;
}


gboolean get_track_info(trackInfo_t *t)
{
	GError *error = NULL;
	GVariant *info = NULL, *val = NULL;
	GVariantIter *iter = NULL;
	gchar *str = NULL, *home = NULL, *dummy = NULL;
	guint32 duint = 0;


	/* get the bulk of the track info */
	info = g_dbus_proxy_call_sync(trackInfoProxy, "GetCurrentTrack", NULL,
																G_DBUS_CALL_FLAGS_NONE, -1, NULL, &error);
	if (info == NULL) {
		g_print("Failed to get track info: %s\n", error->message);
		g_error_free(error);
		return FALSE;
	}

	g_variant_get(info, "(a{sv})", &iter);
	while (g_variant_iter_loop(iter, "{sv}", &str, &val))
		if (g_strcmp0(str, "name") == 0)
			g_variant_get(val, "s", &t->name);
		else if (g_strcmp0(str, "artist") == 0)
			g_variant_get(val, "s", &t->artist);
		else if (g_strcmp0(str, "album") == 0)
			g_variant_get(val, "s", &t->album);
		else if (g_strcmp0(str, "genre") == 0)
			g_variant_get(val, "s", &t->genre);
		else if (g_strcmp0(str, "artwork-id") == 0) {
			home = getenv("HOME");
			g_variant_get(val, "s", &dummy);
			t->artworkId = g_strconcat(home, "/.cache/media-art/90/", dummy, ".jpg", NULL);
			g_free(dummy);
		} else if (g_strcmp0(str, "year") == 0)
			g_variant_get(val, "i", &t->year);
		else if (g_strcmp0(str, "rating") == 0)
			g_variant_get(val, "i", &t->rating);
		else if (g_strcmp0(str, "play-count") == 0)
			g_variant_get(val, "i", &t->playcount);
		else if (g_strcmp0(str, "length") == 0)
			g_variant_get(val, "d", &t->length);

	g_variant_unref(info);


	/* get the current position in track */
	info = NULL;
	info = g_dbus_proxy_call_sync(trackInfoProxy, "GetPosition", NULL,
																G_DBUS_CALL_FLAGS_NONE, -1, NULL, &error);
	if (info == NULL) {
		g_print("Failed to get track info: %s\n", error->message);
		g_error_free(error);
		return FALSE;
	}

	g_variant_get(info, "(u)", &duint);
	t->position = duint/1000;

	g_variant_iter_free(iter);
	g_variant_unref(info);
	g_free(str);

	if (g_strcmp0(t->name, t->oldName) != 0) {
		g_free(t->oldName);
		t->oldName = g_strdup(t->name);
		t->changed = TRUE;
	} else
		t->changed = FALSE;

	return TRUE;
}


void free_track_info(trackInfo_t *t)
{
	g_free(t->name);
	g_free(t->artist);
	g_free(t->album);
	g_free(t->genre);
	g_free(t->artworkId);

	return;
}


gboolean get_player_info(playerInfo_t *i)
{
	GError *error = NULL;
	GVariant *info = NULL;
	gchar *str = NULL;


	/* get wheather player is playing or not */
	info = g_dbus_proxy_call_sync(trackInfoProxy, "GetCurrentState", NULL,
																G_DBUS_CALL_FLAGS_NONE, -1, NULL, &error);
	if (info == NULL) {
		g_print("Failed to get track info: %s\n", error->message);
		g_error_free(error);
		return FALSE;
	}

	g_variant_get(info, "(s)", &str);
	if (g_strcmp0(str, "playing") == 0)
		i->status = TRUE;
	else
		i->status = FALSE;

	g_variant_unref(info);
	g_free(str);


	/* get wheather shuffle is on or off */
	info = NULL;
	info = g_dbus_proxy_call_sync(playerControlsProxy, "GetShuffleMode", NULL,
																G_DBUS_CALL_FLAGS_NONE, -1, NULL, &error);
	if (info == NULL) {
		g_print("Failed to get track info: %s\n", error->message);
		g_error_free(error);
		return FALSE;
	}

	g_variant_get(info, "(s)", &str);
	if (g_strcmp0(str, "off") == 0)
		i->shuffle = FALSE;
	else
		i->shuffle = TRUE;

	g_variant_unref(info);
	g_free(str);


	/* get repeat mode */
	info = NULL;
	info = g_dbus_proxy_call_sync(playerControlsProxy, "GetRepeatMode", NULL,
																G_DBUS_CALL_FLAGS_NONE, -1, NULL, &error);
	if (info == NULL) {
		g_print("Fialed to get track info: %s\n", error->message);
		g_error_free(error);
		return FALSE;
	}

	g_variant_get(info, "(i)", &i->repeat);


	g_variant_unref(info);

	return TRUE;
}


gboolean set_player_pause(void)
{
	GError *error = NULL;


	g_dbus_proxy_call_sync(trackInfoProxy, "Pause", NULL,
													G_DBUS_CALL_FLAGS_NONE, -1, NULL, &error);
	if (error != NULL) {
		g_print("Failed to pause current track: %s\n", error->message);
		g_error_free(error);
		return FALSE;
	}

	return TRUE;
}


gboolean set_player_play(void)
{
	GError *error = NULL;


	g_dbus_proxy_call_sync(trackInfoProxy, "Play", NULL,
													G_DBUS_CALL_FLAGS_NONE, -1, NULL, &error);
	if (error != NULL) {
		g_print("Failed to play current track: %s\n", error->message);
		g_error_free(error);
		return FALSE;
	}

	return TRUE;
}


gboolean set_player_next(void)
{
	GError *error = NULL;


	g_dbus_proxy_call_sync(playerControlsProxy, "Next", g_variant_new("(b)", TRUE),
													G_DBUS_CALL_FLAGS_NONE, -1, NULL, &error);
	if (error != NULL) {
		g_print("Failed to play next track: %s\n", error->message);
		g_error_free(error);
		return FALSE;
	}

	return TRUE;
}


gboolean set_player_prev(void)
{
	GError *error = NULL;


	g_dbus_proxy_call_sync(playerControlsProxy, "Previous", g_variant_new("(b)", TRUE),
													G_DBUS_CALL_FLAGS_NONE, -1, NULL, &error);
	if (error != NULL) {
		g_print("Failed to play previous track: %s\n", error->message);
		g_error_free(error);
		return FALSE;
	}

	return TRUE;
}

gboolean set_player_repeat(gint data)
{
	GError *error = NULL;


	g_dbus_proxy_call_sync(playerControlsProxy, "SetRepeatMode", g_variant_new("(i)", data),
													G_DBUS_CALL_FLAGS_NONE, -1, NULL, &error);
	if (error != NULL) {
		g_print("Failed to set repeat mode: %s\n", error->message);
		g_error_free(error);
		return FALSE;
	}

	return TRUE;
}


gboolean set_player_shuffle(gchar *data)
{
	GError *error = NULL;


	g_dbus_proxy_call_sync(playerControlsProxy, "SetShuffleMode", g_variant_new("(s)", data),
													G_DBUS_CALL_FLAGS_NONE, -1, NULL, &error);
	if (error != NULL) {
		g_print("Failed to set shuffle mode: %s\n", error->message);
		g_error_free(error);
		return FALSE;
	}

	return TRUE;
}


gboolean show_player(void)
{
	GError *error = NULL;
	GDBusProxy *proxy = NULL;


	proxy = g_dbus_proxy_new_sync(connection, G_DBUS_PROXY_FLAGS_NONE, NULL,
																"org.bansheeproject.Banshee",
																"/org/bansheeproject/Banshee/ClientWindow",
																"org.bansheeproject.Banshee.ClientWindow",
																NULL, &error);
	if (proxy == NULL) {
		g_print("Failed to show player window: %s\n", error->message);
		g_error_free(error);
		return FALSE;
	}

	g_dbus_proxy_call_sync(proxy, "Present", NULL, G_DBUS_CALL_FLAGS_NONE, -1, NULL, &error);
	if (error != NULL) {
		g_print("Failed to show player window: %s\n", error->message);
		g_error_free(error);
		return FALSE;
	}

	g_object_unref(proxy);

	return TRUE;
}


gboolean validate_track_info(trackInfo_t *t)
{
	if (!g_utf8_validate(t->album, -1, NULL))
		return FALSE;
	if (!g_utf8_validate(t->artist, -1, NULL))
		return FALSE;
	if (!g_utf8_validate(t->artworkId, -1, NULL))
		return FALSE;
	if (!g_utf8_validate(t->genre, -1, NULL))
		return FALSE;
	if (!g_utf8_validate(t->name, -1, NULL))
		return FALSE;

	return TRUE;
}
