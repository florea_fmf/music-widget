/** window.h -- functions for controlling the gui of the program
	* version 1.0, September 9th, 2012
	*
	* Copyright (C) 2012 Florea Marius Florin
	*
	* This software is provided 'as-is', without any express or implied
	* warranty.  In no event will the authors be held liable for any damages
	* arising from the use of this software.
	*
	* Permission is granted to anyone to use this software for any purpose,
	* including commercial applications, and to alter it and redistribute it
	* freely, subject to the following restrictions:
	*
	*		1. The origin of this software must not be misrepresented; you must not
	*			claim that you wrote the original software. If you use this software
	*			in a product, an acknowledgment in the product documentation would be
	*			appreciated but is not required.
	*		2. Altered source versions must be plainly marked as such, and must not be
	*			misrepresented as being the original software.
	*		3. This notice may not be removed or altered from any source distribution.
	*
	* Florea Marius Florin, florea.fmf@gmail.com
	*/

/** unit responsable for the gui of the program
  */

#ifndef WINDOW_H_INCLUDED
#define WINDOW_H_INCLUDED


#include <gtk/gtk.h>
#include <glib/gprintf.h>
#include <math.h>
#include "data.h"


/** main window size */
#define WINDOW_WIDTH 350
#define WINDOW_HEIGHT 140
/** the background color used for the top level window
 dark grey */
#define WINDOW_BG_COLOR 0.2, 0.2, 0.2
/** the opaquiness (is this even a word?) of the alpha channel */
#define WINDOW_ALPHA 0.8
/** the layout responsable for scrolling label text properties */
#define SCROLL_SPEED 300
#define SCROLL_SIZE_W 243
#define SCROLL_SIZE_H 18
/** how often the program checks dbus to get song's info.
  * time is in milliseconds, don't set it higher than 1000
  * NOTE: (careful) if set too low eats a lot of cpu
  */
#define UPDATE_SPEED 500


/** where on screen to put the window.
  * NOTE: the window will be placed on the rop-right corner
  *				of the window;
  * note to self: make it read the window pos from a config
  *								and place it according do that
	*/
typedef struct {
	gint x, y;
} windowPos_t;


/** widgets displaying info about the playing song.
  * most of them are labels, excepts for
  * "name", "artist", "album", which consists of
  * a layout and a label. The layout is used to
  * scroll the label's text if it doesn't fit
  * in the window's space
  */
typedef struct {
	GtkWidget *name, *artist, *album;
} trackWidgetInfo_t;

typedef struct {
	trackWidgetInfo_t label, layout;
	GtkWidget *genre, *year, *playcount,
						*position, *length, *slider,
						*image, *stars[5];
} trackWidget_t;

/** controls the displayed images on the player's control buttons
 * depending on player current state
 */
typedef struct {
	GtkWidget *playPause, *repeat, *shuffle;
} playerControls_t;

/** 'THE' track structure containing all the information
  * of the song
  * note to self: yeah... let's make a mammouth data structure
	*								and pass it around like a hot potato...
	*								that is sure to do the trick...
  */
typedef struct {
	trackInfo_t track;
	trackWidget_t trackw;
	playerInfo_t playerInfo;
	playerControls_t playerControls;
} track_t;



/** interogates banshee about the current playing
  * song info and displays the info
  */
gboolean update_track_info(track_t *t);

/** sets the widgets with the song info at their proper
  * place in the window and initialeze them with default values
  * returns succes/failure
  * WARNING: meant to be called only once, after the mainVbox is created
  */
gboolean set_widgets_defaults(GtkWidget *mvbox, track_t *t, GdkRGBA *color);

/** used to repaint the window when it's contents change
  */
void on_screen_change_event(GtkWidget *widget, gboolean *supportsAlpha);
gboolean on_draw_event(GtkWidget *widget, gboolean *supportsAlpha);

#endif
