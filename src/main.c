/** main.c -- puts together the program
	* version 1.0, September 9th, 2012
	*
	* Copyright (C) 2012 Florea Marius Florin
	*
	* This software is provided 'as-is', without any express or implied
	* warranty.  In no event will the authors be held liable for any damages
	* arising from the use of this software.
	*
	* Permission is granted to anyone to use this software for any purpose,
	* including commercial applications, and to alter it and redistribute it
	* freely, subject to the following restrictions:
	*
	*		1. The origin of this software must not be misrepresented; you must not
	*			claim that you wrote the original software. If you use this software
	*			in a product, an acknowledgment in the product documentation would be
	*			appreciated but is not required.
	*		2. Altered source versions must be plainly marked as such, and must not be
	*			misrepresented as being the original software.
	*		3. This notice may not be removed or altered from any source distribution.
	*
	* Florea Marius Florin, florea.fmf@gmail.com
	*/


/* this program is so fucked up, not even jesus can save it... */
#include "window.h"


static void set_defaults(track_t *t);


gint main(gint argc, gchar **argv)
{
	track_t track;	/* where the currently playing song info is kept */
	windowPos_t pos;  /* where to put the window */
	GtkWidget *window, *mainVbox;
	GdkRGBA widgetBGColor = {WINDOW_BG_COLOR, 0};
	gboolean supportsAlpha = FALSE;


	/* for whatever reason I wasn't able to start the program
	 * with a delay. So fuck it, here is my delay. */
	/*g_usleep(10000000);*/

	gtk_init(&argc, &argv);


	/* set the main window */
	window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_window_set_title(GTK_WINDOW(window), "Music Widget");
	gtk_window_set_resizable(GTK_WINDOW(window), 0);
	gtk_window_set_decorated(GTK_WINDOW(window), 0);
	gtk_window_stick(GTK_WINDOW(window));
	gtk_window_set_keep_below(GTK_WINDOW(window), 1);
	gtk_window_set_skip_taskbar_hint(GTK_WINDOW(window), 1);
	gtk_window_set_skip_pager_hint(GTK_WINDOW(window), 1);
	gtk_container_set_border_width(GTK_CONTAINER(window), 4);
	gtk_window_set_default_size(GTK_WINDOW(window), WINDOW_WIDTH, WINDOW_HEIGHT);
	g_signal_connect(window, "destroy", G_CALLBACK(gtk_main_quit), NULL);

	pos.x = gdk_screen_get_width(gtk_window_get_screen(GTK_WINDOW(window)))-WINDOW_WIDTH;
	pos.y = 0;
	/* note to self: docs say this function can fail */
	gtk_window_move(GTK_WINDOW(window), pos.x, pos.y);

	/* workaround for changing the buttons colors. The theme always overwrits
	 * whatever color I'm using (either through gtk_widget_override..., or css)
	 * when the window is focused, but shows the way it should when the
	 * window isn't focused. Given that the way the program works doesn't need
	 * focus on the window, this shouldn't be a problem */
	gtk_window_set_accept_focus(GTK_WINDOW(window), 0);

	/* make the bg transparent */
	gtk_widget_set_app_paintable(window, 1);
	g_signal_connect(window, "draw", G_CALLBACK(on_draw_event), &supportsAlpha);
	g_signal_connect(window, "screen-changed", G_CALLBACK(on_screen_change_event), &supportsAlpha);
	on_screen_change_event(window, &supportsAlpha);


	/* in this vbox is packed all other things */
	mainVbox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
	gtk_container_add(GTK_CONTAINER(window), mainVbox);


	/* get and display song info */
	if (!connect_to_dbus())
		return -1;

	set_defaults(&track);
	if(!set_widgets_defaults(mainVbox, &track, &widgetBGColor)) {
			g_print("Failed to initialize program\n");
			return -1;
		}

	if (player_running()) {
		/* if player isn't playing it will crash, because
		 * the data it gets for the track is just random
		 * garbage. */
		get_player_info(&track.playerInfo);
		if (!track.playerInfo.status)
			set_player_play();

		g_timeout_add(UPDATE_SPEED, (GSourceFunc) update_track_info, &track);
	} else
		return -1;


	gtk_widget_show_all(window);

	gtk_main();


	/* clean-up */
	g_free(track.track.oldName);
	gdk_rgba_free(&widgetBGColor);

	if (!disconnect_from_dbus()) {
		g_print("Failed to disconnect from DBus\n");
		return -1;
	}

	return 0;
}


static void set_defaults(track_t *t)
{
	t->track.album = NULL;
	t->track.albumLen = 0;
	t->track.albumScroll = FALSE;
	t->track.albumTag = 0;
	t->track.artist = NULL;
	t->track.artistLen = 0;
	t->track.artistScroll = FALSE;
	t->track.artistTag = 0;
	t->track.artworkId = NULL;
	t->track.changed = TRUE;
	t->track.genre = NULL;
	t->track.length = 0.0;
	t->track.name = NULL;
	t->track.nameLen = 0;
	t->track.nameScroll = FALSE;
	t->track.nameTag = 0;
	t->track.oldName = g_strdup("[...::init::...]");
	t->track.playcount = 0;
	t->track.position = 0.0;
	t->track.rating = 0;
	t->track.year = 0;
	t->track.x.album = 0;
	t->track.x.album2 = 0;
	t->track.x.artist = 0;
	t->track.x.artist2 = 0;
	t->track.x.name = 0;
	t->track.x.name2 = 0;

	return;
}
